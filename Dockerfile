FROM debian
ARG REALVNC_VERSION=6.7.4
ADD https://www.realvnc.com/download/file/vnc.files/VNC-Server-${REALVNC_VERSION}-Linux-x64.deb /tmp/
RUN apt update; apt install -y xfce4 xauth xserver-xorg-video-dummy xserver-xorg-input-void pulseaudio /tmp/VNC-Server-${REALVNC_VERSION}-Linux-x64.deb xfce4-terminal sudo --no-install-recommends; rm /tmp/VNC-Server-${REALVNC_VERSION}-Linux-x64.deb
RUN yes | vncinitconfig -enable-system-xorg; \
echo "\
# make realvnc virtuald not kill session when disconnect, and reconnect to existing instead of start new
ConnectToExisting=1\n\
# disable selected text automatically being copied to clipboard (super annoying)
SendPrimary=0\n\
" > /etc/vnc/config.d/common.custom; \
# vncserver-virtuald does not allow root login
useradd -m -p $(perl -e 'print crypt("password","password")') -G sudo -s /bin/bash admin; \
echo "admin ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/admin
CMD vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA; exec vncserver-virtuald -fg
EXPOSE 5999/tcp
EXPOSE 5999/udp